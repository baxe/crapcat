import numpy as np
from cc import State, View, Feature
from prior import NormalGamma
from model import Gaussian

xa = np.hstack([
    np.random.randn(30) - 3,
    np.random.randn(30) + 3,
    ])

xb = np.hstack([
    np.random.randn(20) - 3,
    np.random.randn(20),
    np.random.randn(20) + 3,
    ])

np.random.shuffle(xb)

def jitter(xs, amt):
    return xs + np.random.randn(*xs.shape) * amt

f1 = Feature(0, jitter(xa, 0.1), Gaussian, NormalGamma(0, 1, 1, 1))
f2 = Feature(1, jitter(xa, 0.1), Gaussian, NormalGamma(0, 1, 1, 1))
f3 = Feature(2, jitter(xa, 0.1), Gaussian, NormalGamma(0, 1, 1, 1))
f4 = Feature(3, jitter(xb, 0.1), Gaussian, NormalGamma(0, 1, 1, 1))
f5 = Feature(4, jitter(xb, 0.1), Gaussian, NormalGamma(0, 1, 1, 1))
f6 = Feature(5, jitter(xb, 0.1), Gaussian, NormalGamma(0, 1, 1, 1))

# view = View([feature])
# for _ in range(100):
#     view.reassign()
#     print(view.asgn.asgn)

state = State([f1, f2, f3, f4, f5, f6])
print(state.asgn.asgn)
for _ in range(100):
    state.update()

print(state.asgn.asgn)
