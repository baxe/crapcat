from scipy.stats import norm


# TODO: Make this a subclass of a more-general random variable class that takes
# observations (data; x) as values and uses them to do posterior updates and so
# on.
class ComponentModel(object):
    """Base class for all component models"""

    def __init__(self, prior, statmodel, params=None, x=None):
        self._params = params or prior.draw(x)
        self._prior = prior
        self._statmodel = statmodel

        # Discrete random variables have PMFs; continuous random variables have
        # PDFs. There has got to be a nicer way of doing this.
        self._is_discrete = hasattr(statmodel, 'pmf')

        if x is not None:
            for xi in x:
                self.insert_datum(xi)

    @property
    def params(self):
        return self._params

    @property
    def suffstats(self):
        raise NotImplementedError

    def insert_datum(self, x):
        raise NotImplementedError

    def remove_datum(self, x):
        raise NotImplementedError

    def likelihood(self, x):
        if self._is_discrete:
            return self._statmodel.pmf(x, **self._params)
        else:
            return self._statmodel.pdf(x, **self._params)

    def log_likelihood(self, x):
        if self._is_discrete:
            return self._statmodel.logpmf(x, **self._params)
        else:
            return self._statmodel.logpdf(x, **self._params)

    def update(self, x=None):
        self._params = self._prior.draw(x)

    def update_nc(self, x=None):
        self._params = self._prior.draw(x)
        # return self._prior.logprior(self._params)
        return 0.0

    def draw(self, size=1):
        return self._statmodel.rvs(**self._params, size=size)

    def log_marginal(self, x=None):
        if x is None:
            return self._prior.logm(suffstats=self.suffstats)
        else:
            return self._prior.logm(x=x)

    def log_postpred(self, y, xk=None):
        if xk is None:
            return self._prior.logp(y, suffstats=self.suffstats)
        else:
            return self._prior.logp(y, xk=xk)


# Component models
# ----------------
class Gaussian(ComponentModel):
    def __init__(self, prior, params=None, x=None):
        self._n = 0
        self._sumx = 0
        self._sumxsq = 0
        super().__init__(prior, norm, params=params, x=x)

    @property
    def n(self):
        return self._n

    @property
    def suffstats(self):
        return self._n, self._sumx, self._sumxsq

    def insert_datum(self, x):
        self._n += 1
        self._sumx += x
        self._sumxsq += x*x

    def remove_datum(self, x):
        self._n -= 1

        if self._n == 0:
            self._sumx = 0
            self._sumxsq = 0
        else:
            self._sumx -= x
            self._sumxsq -= x*x
