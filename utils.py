import numpy as np
from scipy.special import logsumexp


def pflip(ps, is_normed=False, n=1):
    """Categorical draw from weights `ps`."""
    if any(p < 0 for p in ps):
        raise ValueError("ps must contain all positive values")

    bins = np.cumsum(ps)
    if not is_normed:
        bins /= bins[-1]

    x = np.random.rand(n)
    ixs = np.digitize(x, bins)

    if n == 1:
        return int(ixs)
    else:
        return ixs


def lpflip(logps, n=1):
    """Categorical draw from log weights `logps`."""
    ps = np.exp(logps - logsumexp(logps))
    return pflip(ps, is_normed=True, n=n)



