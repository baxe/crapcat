import numpy as np

def crp_draw(n, alpha=1.0):
    asgn = [0]
    counts = [1]
    ncats = 1

    for i in range(1, n):
        weights = np.array(counts + [alpha])
        weights /= np.sum(weights)
        z = np.digitize([np.random.rand()], np.cumsum(weights))[0]

        asgn.append(z)
        if z == ncats:
            ncats += 1
            counts.append(1)
        else:
            counts[z] += 1

        assert(sum(counts) == i + 1)
        assert(ncats == len(counts))
        assert(len(asgn) == i+1)

    assert(len(asgn) == n)
    
    # print(asgn)
    # print(counts)
    # print(ncats)
    return np.array(asgn), counts, ncats


class Assignment:
    def __init__(self, n, alpha=1.0):
        self.asgn, self.counts, self.ncats = crp_draw(n)

    def mark_unused(self, ix):
        z = self.asgn[ix]
        if self.counts[z] == 1:
            del self.counts[z]
            self.ncats -= 1
            self.asgn[self.asgn > z] -= 1
        else:
            self.counts[z] -= 1

        self.asgn[ix] = -1

    def reinsert(self, ix, z):
        assert(self.asgn[ix] == -1)
        self.asgn[ix] = z
        if z == self.ncats:
            self.counts.append(1)
            self.ncats += 1
        else:
            self.counts[z] += 1
