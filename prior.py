from math import log, pi
import numpy as np
from scipy.stats import norm
from scipy.stats import gamma
from scipy.special import gammaln

LOG2 = log(2)
LOGSQRTPI = .5*log(pi)
LOG2PI = log(2*pi)


# TODO: Allow hyperprior so that prior parameters may be resampled
class Prior(object):
    def __init__(self, *params):
        self._params = params

    @property
    def params(self):
        return self._params

    def draw(self, x=None, suffstats=None):
        raise NotImplementedError

    def logm(self, x=None, suffstats=None):
        raise NotImplementedError

    def logp(self, y, x=None, suffstats=None):
        raise NotImplementedError


class NormalGamma(Prior):
    # Taken from:
    # http://www.stats.ox.ac.uk/~teh/research/notes/GaussianInverseGamma.pdf
    def __init__(self, m, r, s, v):
        self._logz0 = NormalGamma.__logz(r, s, v)
        super().__init__(m, r, s, v)

    @staticmethod
    def __logz(r, s, v):
        z = (v+1)/2*LOG2 + LOGSQRTPI - .5*log(r) - (v/2)*log(s) + gammaln(v/2)
        return z

    @staticmethod
    def postparams(n, sumx, sumxsq, m, r, s, v):
        rn = r + n
        vn = v + n
        mn = (r*m + sumx)/rn
        sn = s + sumxsq + r*m*m - rn*mn*mn

        return mn, rn, sn, vn

    @staticmethod
    def suffstats(x):
        if not isinstance(x, np.ndarray):
            return 1, x, x*x
        else:
            n = len(x)
            sumx = np.sum(x)
            sumxsq = np.sum(x**2)

            return n, sumx, sumxsq

    def draw(self, x=None, suffstats=None):
        if x is None and suffstats is None:
            m, r, s, v = self._params
        else:
            if x is None:
                n, sumx, sumxsq = suffstats
            else:
                n, sumx, sumxsq = NormalGamma.suffstats(x)
            m, r, s, v = self.postparams(n, sumx, sumxsq, *self._params)

        rho = gamma.rvs(v/2, scale=2/s)
        mu = norm.rvs(m, (r*rho)**(-0.5))

        # must return mean and standard deviation
        return {'loc': mu, 'scale': 1/rho**.5}

    def logprior(self, params):
        logp = 0.0
        m, r, s, v = self.params
        for param in params:
            loc = params['loc']
            rho = 1./params['scale']**2

            logp += norm.pdf(loc, loc=m, scale=(r*rho)**(-0.5))
            logp += gamma.pdf(rho, v/2, 2/s)
        return logp

    def logm(self, x=None, suffstats=None):
        if x is None:
            n, sumx, sumxsq = suffstats
        else:
            n, sumx, sumxsq = NormalGamma.suffstats(x)

        _, rn, sn, vn = self.postparams(n, sumx, sumxsq, *self._params)
        logzn = NormalGamma.__logz(rn, sn, vn)

        return (-n/2*LOG2PI + logzn - self._logz0)

    def logp(self, y, xk=None, suffstats=None):
        if xk is None:
            n, sumx, sumxsq = suffstats
        else:
            n, sumx, sumxsq = NormalGamma.suffstats(xk)

        mn, rn, sn, vn = self.postparams(n, sumx, sumxsq, *self._params)
        logzn = NormalGamma.__logz(rn, sn, vn)

        mm, rm, sm, vm = self.postparams(1, y, y*y, mn, rn, sn, vn)
        logzm = NormalGamma.__logz(rm, sm, vm)

        return (-.5)*LOG2PI + logzm - logzn
