import random
import numpy as np
from asgn import Assignment
from utils import lpflip


class State:
    def __init__(self, features):
        assert(len(set([f.ident for f in features])) == len(features))
        n = len(features)
        self.asgn = Assignment(n)
        self.features = features
        self.views = []
        feature_store = {}

        for v, feature in zip(self.asgn.asgn, features):
            feature_store[v] = feature_store.get(v, []) + [feature]

        for v in range(self.asgn.ncats):
            view = View(feature_store[v])
            self.views.append(view)

    def remove_col(self, ix):
        v = self.asgn.asgn[ix]
        if self.asgn.counts[v] == 1:
            del self.views[v]
        else:
            self.views[v].remove_col(ix)
        self.asgn.mark_unused(ix)

    def insert_col(self, ix, v, temp_view):
        self.asgn.reinsert(ix, v)
        feature = self.features[ix]
        if v == len(self.views):
            self.views.append(temp_view)
        else:
            self.views[v].insert_col(feature)

    def feature_logm(self, ix, asgn_v):
        feature = self.features[ix]
        feature.reinit(asgn_v)
        return feature.logm()

    def gibbs_col(self, ix):
        feature = self.features[ix]
        self.remove_col(ix)
        logps = np.log(self.asgn.counts + [1.0])
        for v in range(self.asgn.ncats):
            asgn_v = self.views[v].asgn
            logps[v] += self.feature_logm(ix, asgn_v)

        temp_view = View([feature])
        logps[-1] += self.feature_logm(ix, temp_view.asgn)

        v_new = lpflip(logps)
        self.insert_col(ix, v_new, temp_view)

    def reassign(self):
        ixs = list(range(len(self.features)))
        random.shuffle(ixs)
        for ix in ixs:
            self.gibbs_col(ix)

    def update(self):
        self.reassign()
        for view in self.views:
            view.reassign()


class View:
    def __init__(self, features):
        n = features[0].n()
        self.asgn = Assignment(n)
        self.features = features

        for feature in self.features:
            feature.reinit(self.asgn)

    def nrows(self):
        return self.features[0].n()

    def logpred(self, ix, k=None):
        return sum(f.logpred(ix, k) for f in self.features)

    def remove_col(self, ident):
        index = [i for i, f in enumerate(self.features) if f.ident == ident]
        assert len(index) == 1
        del self.features[index[0]]

    def insert_col(self, feature):
        feature.reinit(self.asgn)
        self.features.append(feature)

    def remove_row(self, ix):
        z = self.asgn.asgn[ix]
        for feature in self.features:
            feature.remove_obs(ix, z)
        self.asgn.mark_unused(ix)

    def reinsert_row(self, ix, z):
        for feature in self.features:
            feature.insert_obs(ix, z)
        self.asgn.reinsert(ix, z)

    def gibbs_row(self, ix):
        self.remove_row(ix)
        logps = np.log(self.asgn.counts + [1.0])
        for k in range(self.asgn.ncats):
            logps[k] += self.logpred(ix, k)

        logps[-1] += self.logpred(ix)

        k_new = lpflip(logps)
        self.reinsert_row(ix, k_new)


    def reassign(self):
        ixs = list(range(self.nrows()))
        random.shuffle(ixs)
        for ix in ixs:
            self.gibbs_row(ix)


class Feature:
    def __init__(self, ident, data, cpnt_model, prior):
        self.ident = ident
        self.data = data
        self.prior = prior
        self.cpnt_model = cpnt_model
        self.cpnt_ctor = lambda x=None: self.cpnt_model(self.prior, x=x)

        self.models = []

    def n(self):
        return len(self.data)

    def logpred(self, rowix, k):
        y = self.data[rowix]
        if k is None:
            return self.prior.logm(y)
        else:
            return self.models[k].log_postpred(y)

    def remove_obs(self, ix, z):
        x = self.data[ix]
        self.models[z].remove_datum(x)
        if self.models[z].n == 0:
            del self.models[z]

    def insert_obs(self, ix, z):
        x = self.data[ix]
        if z == len(self.models):
            self.models.append(self.cpnt_ctor())
        if z > len(self.models):
            import pdb; pdb.set_trace()
        self.models[z].insert_datum(x)

    def reinit(self, asgn):
        self.models = []

        for k in range(asgn.ncats):
            self.models.append(self.cpnt_ctor())

        for x, z in zip(self.data, asgn.asgn):
            self.models[z].insert_datum(x)

    def logm(self):
        return sum(m.log_marginal() for m in self.models)
